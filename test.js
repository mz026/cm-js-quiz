var Animal = function(name) {
  this.name = name;
};
Animal.prototype.getName = function() {
  return this.name;
};
Animal.prototype.greeting = function() {
  return "Greeting from: " + this.name;
};
Animal.prototype.attack = function() {
  return 'rarrrrr';
}

// Dog class:
// Implement a class `Dog`, which is a subclass of Animal, so that we can do:
//
// var dog = new Dog('Lucky');
// dog.greeting(); // => 'Greeting from: Lucky'
//
// dog.attack(); // => 'wooof';
//
// dog.attackWithCount(); // => 'wooof * 1'
// dog.attackWithCount(); // => 'wooof * 2'
// dog.attackWithCount(); // => 'wooof * 3'
